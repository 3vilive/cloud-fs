package internal

import (
	"crypto/sha256"
	"encoding/hex"
	"io"
)

func HexSha256File(file io.Reader) (string, error) {
	h := sha256.New()
	if _, err := io.Copy(h, file); err != nil {
		return "", err
	}

	sum := h.Sum(nil)
	hexResult := make([]byte, hex.EncodedLen(len(sum)))
	hex.Encode(hexResult, sum)

	return string(hexResult), nil
}

