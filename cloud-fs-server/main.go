package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"path"

	"github.com/gin-gonic/gin"

	"cloud-fs/cloud-fs-server/internal"
)

const (
	defaultFSDir       = "./fs"
	defaultHost        = "0.0.0.0"
	defaultPort        = 13888
	defaultAccessToken = "access7"
)

type options struct {
	FSDir       string
	Host        string
	Port        int
	AccessToken string
}

var opts *options

func initOpts() {
	if opts != nil {
		return
	}

	opts = &options{}
	flag.StringVar(&opts.FSDir, "fsdir", defaultFSDir, "specify directory of file storage")
	flag.StringVar(&opts.Host, "host", defaultHost, "host to bind")
	flag.IntVar(&opts.Port, "port", defaultPort, "port to listen")
	flag.StringVar(&opts.AccessToken, "token", defaultAccessToken, "access token")
	flag.Parse()
}

func validationTokenMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		accessToken := c.Query("token")

		if accessToken != opts.AccessToken {
			c.JSON(http.StatusForbidden, gin.H{
				"ret": -1,
				"msg": "forbidden",
			})
			c.Abort()
			return
		}

		c.Next()
	}
}

func buildDownloadURL(c *gin.Context, fileName string) string {
	return fmt.Sprintf("http://%s/v1/download/file/%s?token=%s",
		c.Request.Host,
		fileName,
		c.Query("token"),
	)
}

func onUploadFileV1(c *gin.Context) {
	uploadFile, err := c.FormFile("file")
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"ret": -1,
			"msg": err.Error(),
		})
		return
	}

	log.Printf("OnUploadImageV1 FileName: %s Header: %s Size: %d (bytes)\n",
		uploadFile.Filename, uploadFile.Header, uploadFile.Size)

	file, err := uploadFile.Open()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"ret": -1,
			"msg": err.Error(),
		})
		return
	}
	defer func() {
		err := file.Close()
		if err != nil {
			fmt.Println(err)
		}
	}()

	fileSha256Hex, err := internal.HexSha256File(file)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"ret": -1,
			"msg": err.Error(),
		})
		return
	}

	saveFileName := fmt.Sprintf("%s%s", fileSha256Hex, path.Ext(uploadFile.Filename))
	saveFilePath := path.Join(opts.FSDir, saveFileName)
	if err = c.SaveUploadedFile(uploadFile, saveFilePath); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"ret": -1,
			"msg": err.Error(),
		})
		return
	}

	downloadURL := buildDownloadURL(c, saveFileName)
	c.JSON(http.StatusOK, gin.H{
		"ret":          0,
		"msg":          "ok",
		"download_url": downloadURL,
	})
	return
}

func initRouter() *gin.Engine {
	ginEngine := gin.Default()

	// middleware
	ginEngine.Use(validationTokenMiddleware())

	// api group
	gUploadV1 := ginEngine.Group("/v1/upload")
	gUploadV1.POST("/file", onUploadFileV1)

	gDownloadV1 := ginEngine.Group("/v1/download/file")
	gDownloadV1.Static("", opts.FSDir)

	return ginEngine
}

func main() {
	initOpts()
	fmt.Printf("opts: %v\n", opts)

	r := initRouter()
	addr := fmt.Sprintf("%s:%d", opts.Host, opts.Port)
	err := r.Run(addr)
	if err != nil {
		panic(err)
	}
}
